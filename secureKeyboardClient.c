/* 
 * File:   secureKeyboardClient.c
 * Author: Dušan Jenčík
 *
 * Created on 10. května 2014, 10:29
 */

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>

#include "secureKeyboardClient.h"
#include "card_controller.h"

int sockfd = 0;

int validateUser(int gateID, int userID, int pin) {
    if (sockfd == 0 && !conn()) exit(1);
    char* buff = malloc(sizeof (char)*23);
    asprintf(&buff, "checkaccess %d %d %d\n", gateID, userID, pin);
    if (writeToServer(buff)) {
        buff = readFromServer();
        puts(buff);
        if (strstr(buff, "OK")) return 1;
    }
    return 0;
}

int writeToServer(char* str) {
    if (verbose_flag == 2)
        printf("Write: %s\n", str);
    int n = write(sockfd, str, (unsigned long) strlen(str));
    if (n < 0) {
        if (verbose_flag)
            fprintf(stderr, "\n Error : Write failed\n");
        return 0;
    }
    return 1;
}

char* readFromServer() {
    char *buffer = malloc(sizeof (char) * 256);
    bzero(buffer, 256);
    int n = read(sockfd, buffer, 255);
    if (n < 0) {
        if (verbose_flag)
            fprintf(stderr, "\n Error : Read failed\n");
        return "";
    }
    if (verbose_flag == 2)
        printf("Readed: %s\n", buffer);
    return buffer;
}

int conn() {
    struct sockaddr_in serv_addr;
    bzero((char *) &serv_addr, sizeof (serv_addr));
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        if (verbose_flag)
            fprintf(stderr, "\n Error : Could not create socket \n");
        return 0;
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(55556);
    serv_addr.sin_addr.s_addr = htonl(0x7F000001); // localhost

    return !(connect(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0);
}