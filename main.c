/* 
 * File:   main.c
 * Author: Dušan Jenčík
 *
 * Created on 8. května 2014, 11:18
 */

#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>

#include "chmods.h"
#include "card_controller.h"
#include "secureKeyboardClient.h"

int verbose_flag;
int hashCount = 0;

void printMenu() {
    puts("usage: ./emulator_karty <parameters>\n\n"
            "   -v  --verbose   verbose output\n"
            "   -f  --force     detailed verbose output\n"
            "   -h  --help      show this screen\n\nmore in manual...\n"
            );
    exit(1);
}

void options(int argc, char *argv[]) {
    while (1) {
        static struct option long_options[] = {
            {"verbose", no_argument, &verbose_flag, 1},
            {"force", no_argument, &verbose_flag, 2},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}
        };
        int option_index = 0;
        int c = getopt_long(argc, argv, "vfh",
                long_options, &option_index);
        if (c == -1) break;
        if (c == 'v' && verbose_flag != 2) verbose_flag = 1;
        if (c == 'f') verbose_flag = 2;
        if (c == 'h') printMenu();
    }
}

void LEDSnake() {
    runWrite(BUS_KBD_WR_o, 0x80); // beep
    int i, j;
    for (j = 0; j < 3; j++) {
        for ((j % 2 == 0) ? (i = 0x80) : (i = 0x02);
                (j % 2 == 0) ? (i != 0x00) : (i != 0x80);
                (j % 2 == 0) ? (i >>= 0x01) : (i <<= 0x01)) {
            setLED(i);
            waiting(33000);
        }
    }
    setLED(0x00);
}

void LEDLoading() {
    runWrite(BUS_KBD_WR_o, 0x80); // beep
    int i, j = 0x80;
    for (i = 0x80; i != 0xff; i += (j >>= 0x01)) {
        setLED(i);
        waiting(125000);
    }
    setLED(i);
    runWrite(BUS_KBD_WR_o, 0x00);
    waiting(500000);
    setLED(0x00);
}

int readFromKeyboard(char* str, int maxLength) {
    char* id;
    asprintf(&id, "Zadejte %s:", str);
    setLCD(id);
    char* buff = malloc(sizeof (char) * (maxLength + 1));
    int readed;
    char read;
    for (readed = 0; readed < maxLength; readed++) {
        read = readKeyboard();
        if (read == '#' && readed == 0 && ++hashCount == 3) {
            return -1; // konec aplikace
        }
        if (read == '#') {
            readed = -1;
            setLED(0xff);
            setLCD(id);
            waiting(100000);
            setLED(0x00);
            continue;
        }
        buff[readed] = read;
        if (maxLength == 4) setLCDToPos('*', 16 + readed);
        else setLCDToPos(read, 16 + readed);
    }
    buff[readed] = 0;
    return atoi(buff);
}

void validationProcess() {
    hashCount = 0;
    setLED(0x00);
    int gateID, userID, pin;
    setLCD("   Dobry  den   ");
    waiting(1000000);
    gateID = readFromKeyboard("gateID", 2);
    waiting(300000);
    if (gateID == -1) return;
    userID = readFromKeyboard("userID", 2);
    waiting(300000);
    if (userID == -1) return;
    pin = readFromKeyboard("pin", 4);
    waiting(300000);
    if (pin == -1) return;

    if (validateUser(gateID, userID, pin)) {
        setLED(0xdb);
        setLCD("Uzivatel overen.");
        beep(500);
    } else {
        setLED(0x24);
        setLCD(" Neautorizovany     pristup     ");
        beep(1000);
        waiting(300000);
        beep(1000);
        waiting(300000);
        beep(1000);

        if (verbose_flag)
            fprintf(stderr, "Validation failed\n");
    }
    waiting(1000000);
    validationProcess();
}

int main(int argc, char *argv[]) {
    system("clear");
    options(argc, argv);
    if (conn()) {
        if (verbose_flag)
            puts("Connected to GateCentral");
        if (isPciSlotInSystem()) {
            if (verbose_flag == 1)
                puts("--------------- verbosing ---------------");
            if (verbose_flag == 2)
                puts("----------- detailed verbosing ----------");
            LEDLoading();
            validationProcess();
            setLCD("    Dekujeme      nashledanou.  ");

            LEDSnake();
        } else
            noCard();
        if (verbose_flag)
            puts("-----------------------------------------");
        destroy();
    } else {
        fprintf(stderr, "Program can not be run. It require GateCentral server!\n");
    }
    return 0;
}

//+TODO: cteni klavesnice


