/* 
 * File:   secureKeyboardClient.h
 * Author: Dušan Jenčík
 *
 * Created on 15. května 2014, 19:04
 */

#ifndef SECUREKEYBOARDCLIENT_H
#define	SECUREKEYBOARDCLIENT_H

extern int verbose_flag;
int validateUser(int gateID, int userID, int pin);
int writeToServer(char* str);
char* readFromServer();
int conn();

#endif	/* SECUREKEYBOARDCLIENT_H */

