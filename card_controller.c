/* 
 * File:   card_controller.c
 * Author: Dušan Jenčík
 *
 * Created on 9. května 2014, 17:48
 */

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <malloc.h>

#include "chmods.h"
#include "card_controller.h"

void waiting(int microsec) {
    usleep(microsec);
}

void noCard() {
    puts("Card is not plugged in!");
    exit(1);
}

void destroy() {
    if (base != NULL) {
        runWrite(BUS_LED_WR_o, 0x00); // clear LED
        runWrite(BUS_LCD_INST_o, CHMOD_LCD_CLR); // clear LCD
        waiting(10);
        runWrite(BUS_LCD_INST_o, CHMOD_LCD_DON);
        runWrite(0x80, 0x00); // shutdown
    }
    enablePCI(0);
}

int isWantedCard(char *file) {
    FILE *fp = fopen(file, "rb");
    char nameOfDevice[4] = {0};
    int i;

    if (fp == NULL) {
        if (verbose_flag)
            fprintf(stderr, "Failed to open file %s\n", file);
        return 0;
    }

    fread(nameOfDevice, 4, 1, fp);
    fseek(fp, 18, SEEK_SET);
    fread(addressOfCard, 2, 1, fp);

    fclose(fp);

    int ok = 1;
    for (i = 0; i < 4; i++) ok &= nameOfDevice[i] == cardName[i];

    printf("card %x%x:%x%x at 0x%x\n",
            nameOfDevice[1], nameOfDevice[0], nameOfDevice[3], nameOfDevice[2],
            (addressOfCard[0] << 16));
    return ok;
}

int enablePCI(int enableD) {
    char cen = enableD != 0 ? '1' : '0';
    char * str;
    asprintf(&str, "/sys/bus/pci/devices/0000:00:%s/enable", cardBusName);
    if (verbose_flag == 2)
        printf("%s < %d\n", str, enableD);
    int enable = open(str, O_WRONLY);
    if (enable == -1) {
        fprintf(stderr, "pci is not enabled");
        exit(1);
        return 0;
    }
    write(enable, &cen, 1);
    close(enable);
    return 1;
}

int isPciSlotInSystem() {
    char * dname = "/proc/bus/pci/";
    int exists = 0;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(dname)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_name[0] != '.' && ent->d_name[0] != 'd') {
                char* file;
                asprintf(&file, "%s%s/", dname, ent->d_name);
                if (isPciCardInSystem(file)) {
                    exists = 1;
                    break;
                }
            }
        }
        closedir(dir);
    }
    return exists;
}

void doMap(int *soubor) {
    base = mmap(NULL, 0x10000, PROT_WRITE | PROT_READ, MAP_SHARED,
            *soubor, (addressOfCard[0] << 16));
    if (base == MAP_FAILED) {
        fprintf(stderr, "Unable to map device.");
        exit(1);
    }
    runWrite(0x80, 0xF0); // zapne napájení
    waiting(10);

    // inicializace LCD
    runWrite(BUS_LCD_INST_o, CHMOD_LCD_MOD);
    waiting(10);
    runWrite(BUS_LCD_INST_o, CHMOD_LCD_MOD);
    waiting(10);
    runWrite(BUS_LCD_INST_o, CHMOD_LCD_CLR);
    waiting(10);
    runWrite(BUS_LCD_INST_o, CHMOD_LCD_DON);
    waiting(10);
}

int isPciCardInSystem(char * dname) {
    int exists = 0;

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(dname)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_name[0] != '.') {
                char* file;
                asprintf(&file, "%s%s", dname, ent->d_name);
                if (verbose_flag)
                    printf("Testing device: %s\n", ent->d_name);
                if (isWantedCard(file)) {
                    cardBusName = malloc(sizeof (char) * 4);
                    strcpy(cardBusName, ent->d_name);
                    if (enablePCI(1)) {
                        exists = 1;
                        int soubor = open("/dev/mem", O_RDWR | O_SYNC);
                        if (soubor == -1) {
                            fprintf(stderr, "/dev/mem not found");
                            exit(1);
                        }
                        doMap(&soubor);
                    }
                    break;
                }
            }
        }
        closedir(dir);
    }
    return exists;
}

void runWrite(unsigned int addr, unsigned int data) {
    *(base + CTRL) = POWER_UP;
    *(base + 0x8000 + ADDR) = addr;
    *(base + CTRL) = POWER_UP & WR0;
    *(base + 0x8000 + WRITE) = data;
    *(base + CTRL) = POWER_UP & WR0 & CS0;
    waiting(10000);
    *(base + CTRL) = POWER_UP & WR0;
    *(base + CTRL) = POWER_UP;
}

unsigned int runRead(unsigned int addr) {
    *(base + CTRL) = POWER_UP;
    *(base + CTRL) = POWER_UP & RD0;
    *(base + 0x8000 + ADDR) = addr;
    *(base + CTRL) = POWER_UP & RD0 & CS0;
    waiting(10000);
    unsigned int output = *(base + 0x8000 + READ);
    *(base + CTRL) = POWER_UP & RD0;
    *(base + CTRL) = POWER_UP;
    return output;
}

void setLED(unsigned int hex) {
    if (verbose_flag == 2) {
        unsigned int i = hex;
        size_t bits = 8;
        char * str = malloc(bits + 1);
        str[bits] = 0;
        for (; bits--; i >>= 1) str[bits] = i & 1 ? '1' : '0';
        printf("set LED: %s\n", str);
    }
    runWrite(BUS_LED_WR_o, hex);
}

void setLCDToPos(const char c, int pos) {
    while (runRead(BUS_LCD_STAT_o) == CHMOD_LCD_BF)
        waiting(10);
    waiting(10);
    runWrite(BUS_LCD_INST_o, CHMOD_LCD_POS + (pos / 16)*0x40 + (pos % 16));
    waiting(10);
    runWrite(BUS_LCD_WDATA_o, c);
    if (verbose_flag == 1) {
        if (pos % 16 == 0) printf("\n");
        printf("%c", c);
    }
    if (verbose_flag == 2) {
        if (pos % 16 == 0) printf("\n");
        printf("key diplayed: %c\n", c);
    }
}

void setLCD(const char* str) {
    runWrite(BUS_LCD_INST_o, CHMOD_LCD_CLR);
    waiting(10);
    runWrite(BUS_LCD_INST_o, CHMOD_LCD_DON);
    if (verbose_flag)
        printf("set LCD:");
    int i;
    for (i = 0; i < strlen(str); i++) setLCDToPos(str[i], i);
    if (verbose_flag) printf("\n");
}

/**
 * 
 * @param length in ms
 */
void beep(unsigned int length) {
    if (verbose_flag == 2) printf("beep for %d ms\n", (length));
    runWrite(BUS_KBD_WR_o, 0x80);
    waiting(length * 1000);
    runWrite(BUS_KBD_WR_o, 0x00);
}

char decodeKeys(int key) {
    //printf("key %x\n", key);
    switch (key) {
        case KEY_0: return '0';
        case KEY_1: return '1';
        case KEY_2: return '2';
        case KEY_3: return '3';
        case KEY_4: return '4';
        case KEY_5: return '5';
        case KEY_6: return '6';
        case KEY_7: return '7';
        case KEY_8: return '8';
        case KEY_9: return '9';
        case KEY_HASH: return '#';
        default: return '-';
    }
}

char keyboardUpdate(int read) {
    char key = decodeKeys(read);
    //printf("decoded %c\n", key);
    if (key == '-' || lastKey != 0) {
        lastKey = 0;
        return 0;
    }
    lastKey = key;
    waitingForNextKey = 0;
    return key;
}

char readKeyboard() {
    char readed;
    for (; !((lastKey != 0) && !waitingForNextKey); waiting(50000)) {
        runWrite(BUS_KBD_WR_o, KEYBOARD_COL1);
        char col1 = POWER_UP - runRead(BUS_KBD_RD_o);
        runWrite(BUS_KBD_WR_o, KEYBOARD_COL2);
        char col2 = POWER_UP - runRead(BUS_KBD_RD_o);
        runWrite(BUS_KBD_WR_o, KEYBOARD_COL3);
        char col3 = POWER_UP - runRead(BUS_KBD_RD_o);

        readed = keyboardUpdate((((col3 << 8) + col2) << 8) + col1);
    }
    beep(50);
    waitingForNextKey = 1;
    return readed;
}