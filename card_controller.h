/* 
 * File:   card_controller.h
 * Author: Dušan Jenčík
 *
 * Created on 9. května 2014, 17:53
 */

#ifndef CARD_CONTROLLER_H
#define	CARD_CONTROLLER_H

#define CTRL 0x8080
#define ADDR 0x60
#define READ 0x40
#define WRITE 0x20

#define POWER_UP 0xff
#define CS0 0xbf
#define WR0 0xfd
#define RD0 0xfe

#define KEY_1 0x1
#define KEY_2 0x100
#define KEY_3 0x10000
#define KEY_4 0x2
#define KEY_5 0x200
#define KEY_6 0x20000
#define KEY_7 0x4
#define KEY_8 0x400
#define KEY_9 0x40000
#define KEY_0 0x800
#define KEY_STAR 0x8
#define KEY_HASH 0x80000

#define KEYBOARD_COL1 0x03
#define KEYBOARD_COL2 0x05
#define KEYBOARD_COL3 0x06

extern int verbose_flag; // příznak výpisů
static char cardName[4] = {0x72, 0x11, 0x32, 0x1f}; // hledaná karta
static int addressOfCard[1] = {0}; // adresa karty
static char* cardBusName;
unsigned char * base;
static int stable = 1;
static char lastKey;
static int waitingForNextKey;

void noCard();
int isPciCardInSystem();
void runWrite(unsigned int addr, unsigned int data);
unsigned int runRead(unsigned int addr);
void setLED(unsigned int hex);
void setLCDToPos(const char c, int pos);
void setLCD(const char* str);
void beep(unsigned int length);
char readKeyboard();
void destroy();

#endif	/* CARD_CONTROLLER_H */

