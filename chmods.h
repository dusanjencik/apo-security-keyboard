/* 
 * File:   chmods.h
 * Author: dj
 *
 * Created on 9. května 2014, 14:12
 */

#ifndef CHMODS_H
#define	CHMODS_H

/* MA_KBD3 register mapping on 8-bit emulated bus */
#define BUS_LCD_INST_o  0x0000 /* WR LCD instruction */
#define BUS_LCD_STAT_o  0x0001 /* RD LCD status */
#define BUS_LCD_WDATA_o 0x0002 /* WR LCD data to display or CG */
#define BUS_LCD_RDATA_o 0x0003 /* RD LCD RAM data */
#define BUS_LED_WR_o    0x0001 /* WR data to show on LED diodes */
#define BUS_KBD_WR_o    0x0003 /* WR data to keyboard scan register */
#define BUS_KBD_RD_o    0x0000 /* RD data returned from keyboard matrix */

//----------------------------------

/* Status flags in LCD_STAT */
#define CHMOD_LCD_BF   0x80  /* busy flag */

/* Control codes for LCD_INST */
#define CHMOD_LCD_POS  0x80  /* set cursor position */
#define CHMOD_LCD_CLR  0x01  /* clear display */
#define CHMOD_LCD_HOME 0x02  /* cursor home */
#define CHMOD_LCD_MOD  0x38  /* mode dual line, 8 bits */
#define CHMOD_LCD_NROL 0x10  /* not scrolling display */
#define CHMOD_LCD_DON  0x0C  /* display on */
#define CHMOD_LCD_CON  0x0A  /* cursor on */
#define CHMOD_LCD_BON  0x09  /* blink on */
#define CHMOD_LCD_NSH  0x04  /* motion */


#endif	/* CHMODS_H */

